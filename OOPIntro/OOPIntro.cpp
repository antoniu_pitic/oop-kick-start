// 
#include <iostream>
using namespace std;

#pragma region definire_clase

class IShowable {
public:
	virtual void Show() = 0;
};

class DestructibleObject {
protected:
	int X, Y;
	char poza[300];
	int life;

public:
	DestructibleObject() {
		X = Y = 0;
		// poza = "cale implicita, poza cu eroare"
		life = 100;
	}

	DestructibleObject(int X, int Y, int life) {
		this->X = X;
		this->Y = Y;
		this->life = life;
	}


};

class Factory: public DestructibleObject, public IShowable {
protected:
	int armor;
	int unitProductionSpeed;
public:
	Factory():DestructibleObject() {
		armor = 10;
		unitProductionSpeed = 60;
	}

	Factory(int X, int Y, int life, int armor, int unitProductionSpeed)
		:DestructibleObject(X,Y,life) 
	{
		this->armor = armor;
		this->unitProductionSpeed = unitProductionSpeed;
	}

	void Show() {
		cout << "Sunt o fabrica la " << X << "," << Y;
		cout << " cu armura " << armor << " si  viteza productie ";
		cout << unitProductionSpeed << endl;
	}
};

class Humans : public DestructibleObject {
protected:
	bool stealth;

public:
	Humans(int X, int Y, int life, bool stealth):DestructibleObject(X,Y,life) {
		this->stealth = stealth;
	}

	void Move(int x, int y) {
		cout << "Moving from " << X << "," << Y << " to ";
		X = x;
		Y = y;
		cout << X << "," << Y << endl;
	}
	void Sleep(int time) {
		life += time;
	}

};

class Marine : public Humans, public IShowable {
public:
	Marine(int X, int Y, int life, bool stealth):Humans(X,Y,life,stealth) {

	}

	void Defend() {
		cout << "Im defending at" << X << "," << Y << endl;
	}
	void Attack(int x, int y) {
		cout << "<==== ATTACK SECVENCE =======>" << endl;
		Move(x, y);
		cout << "Im attaking at " << X << "," << Y << endl;
	}

	void Show() {
		cout << "Sunt un soldat la " << X;
		cout << "," << Y << " cu viata: " << life << endl;
	}
};

#pragma endregion

int main()
{

	Factory f;
	Factory f2(50, 50, 2500, 14, 30);

	Marine m1(78, 44, 50, true);
	Marine m2(46, 22, 50, true);
	Marine m3(18, 5, 50, true);

	IShowable* unit[100];
	int noOfUnits = 5;
	unit[0] = &f;
	unit[1] = &f2;
	unit[2] = &m1;
	unit[3] = &m2;
	unit[4] = &m3;

	unit[noOfUnits++] = new Marine(101, 202, 15, false);

	for (int i = 0; i < noOfUnits; i++) {
		unit[i]->Show();
	}

}

